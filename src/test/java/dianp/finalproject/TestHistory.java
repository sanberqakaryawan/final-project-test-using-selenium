package dianp.finalproject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;


/**
 * @author Dian Prasetyo
 * This class is for history page testing
 * */
public class TestHistory {

    WebDriver driver;
    @BeforeClass
    public  void  openBrowser(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(10000));
        login();
    }

    public void login(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = driver.findElement(By.xpath("/html/body/section/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("/html/body/section/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();
    }
    @BeforeMethod
    public void goToHome(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/");
    }


    /**
     * @throws InterruptedException
     * See history when not make any appointment yet
     */
    @Test(priority = 0)
    public  void TC_HIS_01() throws InterruptedException {
        WebElement menu = driver.findElement(By.id("menu-toggle"));
        WebElement historyMenu = driver.findElement(By.xpath("/html/body/nav/ul/li[3]/a"));

        menu.click();
        Thread.sleep(2000);
        historyMenu.click();

        String actual = driver.findElement(By.xpath("/html/body/section/div/div[1]/div/h2")).getText();
        String expected = "History";

        Assert.assertEquals(actual, expected);
    }

    /**
     * @throws InterruptedException
     * See history when already make appointment
     */
    @Test(priority = 1)
    public  void TC_HIS_02() throws InterruptedException {
        makeAppointment();
        WebElement menu = driver.findElement(By.id("menu-toggle"));
        WebElement historyMenu = driver.findElement(By.xpath("/html/body/nav/ul/li[3]/a"));

        menu.click();
        Thread.sleep(2000);
        historyMenu.click();

        String actual = driver.findElement(By.xpath("/html/body/section/div/div[1]/div/h2")).getText();
        String expected = "History";

        Assert.assertEquals(actual, expected);
    }

    public void makeAppointment(){
        Select drpFacility = new Select(driver.findElement(By.id("combo_facility")));
        WebElement readmission = driver.findElement(By.id("chk_hospotal_readmission"));
        WebElement program = driver.findElement(By.id("radio_program_medicaid"));
        WebElement visitDate = driver.findElement(By.id("txt_visit_date"));
        WebElement comment = driver.findElement(By.id("txt_comment"));
        WebElement btnBook = driver.findElement(By.id("btn-book-appointment"));

        drpFacility.selectByValue("Hongkong CURA Healthcare Center");
        if(!readmission.isSelected()){
            readmission.click();
        }
        program.click();
        visitDate.sendKeys("28/12/2022");
        comment.sendKeys("This is dummy comment");
        btnBook.click();

    }

    @AfterClass
    public  void closeBrowser() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();

    }
}
