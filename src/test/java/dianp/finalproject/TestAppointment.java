package dianp.finalproject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Duration;

/**
 * @author Dian Prasetyo
 * This class is for appointment page testing
 * */
public class TestAppointment {

    WebDriver driver;
    @BeforeClass
    public  void  openBrowser(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(10000));
        login();
    }

    public void login(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = driver.findElement(By.xpath("/html/body/section/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("/html/body/section/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();
    }
    @BeforeMethod
    public void goToHome(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/");
    }


    /**
     * @throws InterruptedException
     * Book appointment by fill mandatory field only
     */
    @Test(priority = 0)
    public void TC_MAP_01() throws InterruptedException {
        WebElement visitDate = driver.findElement(By.id("txt_visit_date"));
        WebElement btnBook = driver.findElement(By.id("btn-book-appointment"));


        visitDate.sendKeys("28/12/2022");

        Thread.sleep(2000);
        btnBook.click();


        String actual = driver.findElement(By.xpath("/html/body/section/div/div/div[1]/h2")).getText();
        String expected = "Appointment Confirmation";

        Assert.assertEquals(actual, expected);
        Thread.sleep(2000);

    }


    /**
     * @throws InterruptedException
     * Book appointment by fill all field
     */
    @Test(priority = 1)
    public void TC_MAP_02() throws InterruptedException {
        Select drpFacility = new Select(driver.findElement(By.id("combo_facility")));
        WebElement readmission = driver.findElement(By.id("chk_hospotal_readmission"));
        WebElement program = driver.findElement(By.id("radio_program_medicaid"));
        WebElement visitDate = driver.findElement(By.id("txt_visit_date"));
        WebElement comment = driver.findElement(By.id("txt_comment"));
        WebElement btnBook = driver.findElement(By.id("btn-book-appointment"));

        drpFacility.selectByValue("Hongkong CURA Healthcare Center");
        if(!readmission.isSelected()){
            readmission.click();
        }
        program.click();
        Thread.sleep(2000);
        visitDate.sendKeys("28/12/2022");
        comment.sendKeys("This is dummy comment");
        Thread.sleep(2000);
        btnBook.click();


        String actual = driver.findElement(By.xpath("/html/body/section/div/div/div[1]/h2")).getText();
        String expected = "Appointment Confirmation";

        Assert.assertEquals(actual, expected);
        Thread.sleep(2000);

    }

    /**
     * @throws InterruptedException
     * Book Appointment without fill mandatory field
     */
    @Test(priority = 2)
    public void TC_MAP_03() throws InterruptedException {
        WebElement btnBook = driver.findElement(By.id("btn-book-appointment"));

        Thread.sleep(2000);
        btnBook.click();

        try {
            Assert.assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Value is required and can't be empty[\\s\\S]*$"));
        } catch (Error e) {
            e.printStackTrace();
        }

        Thread.sleep(2000);

    }


    @AfterClass
    public  void closeBrowser() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();

    }
}
