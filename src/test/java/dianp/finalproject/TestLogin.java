package dianp.finalproject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Duration;

/**
 * @author Dian Prasetyo
 * This class is for login feature testing
 * */
public class TestLogin {

    WebDriver driver;


    @BeforeClass
    public  void  openBrowser(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(10000));
    }

    @AfterMethod
    public void logout() throws InterruptedException {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(2000);
    }

    /**
     * Test login using valid username and valid password
     */
    @Test
    public void TC_LOG_01(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = driver.findElement(By.xpath("/html/body/section/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("/html/body/section/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        String actualResult = driver.findElement(By.id("btn-book-appointment")).getText();
        String expected = "Book Appointment";
        Assert.assertEquals(actualResult,expected);

    }

    /**
     * Test login using invalid username and valid password
     */
    @Test
    public void TC_LOG_02(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = "username salah";
        String password = "ThisIsNotAPassword";

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        String actualResult = driver.findElement(By.className("text-danger")).getText();
        String expected = "Login failed! Please ensure the username and password are valid.";
        Assert.assertEquals(actualResult,expected);
    }

    /**
     * Test login using valid username and invalid password
     */
    @Test
    public void TC_LOG_03(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");


        String username = "John Doe";
        String password = "password salah";


        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        String actualResult = driver.findElement(By.className("text-danger")).getText();
        String expected = "Login failed! Please ensure the username and password are valid.";
        Assert.assertEquals(actualResult,expected);
    }

    /**
     * Test login using invalid username and invalid password
     */
    @Test
    public void TC_LOG_04(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = "username salah";
        String password = "password salah";

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        String actualResult = driver.findElement(By.className("text-danger")).getText();
        String expected = "Login failed! Please ensure the username and password are valid.";
        Assert.assertEquals(actualResult,expected);
    }


    @AfterClass
    public  void closeBrowser() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();

    }
}
